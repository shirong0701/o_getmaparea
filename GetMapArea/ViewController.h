//
//  ViewController.h
//  GetMapArea
//
//  Created by Billy on 20/03/15.
//  Copyright (c) 2015 Billy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapView.h"
#import "Place.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate, MKMapViewDelegate>
{
    MapView *   mapView;
    Place   *   fromLocation;
    Place   *   toLocation;
    CLLocationManager *locmanager;
}

@property (strong, nonatomic) CLLocationManager * locmanager;
@property (strong, nonatomic) Place * fromLocation;
@property (strong, nonatomic) Place * toLocation;
@property (nonatomic, strong) MapView * mapView;


@end

