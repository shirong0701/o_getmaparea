//
//  ViewController.m
//  GetMapArea
//
//  Created by Billy on 20/03/15.
//  Copyright (c) 2015 Billy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize fromLocation, toLocation;
@synthesize mapView;
@synthesize locmanager;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    mapView = [[MapView alloc] initWithFrame:
               CGRectMake(0, 44, self.view.frame.size.width,460-44)] ;
    [self.view addSubview:mapView];
    
    fromLocation = [[Place alloc] init] ;
    fromLocation.name = @"fromLocation";
    fromLocation.description = @"fromLocation";
    fromLocation.latitude = 0;
    fromLocation.longitude = 0;
    
    toLocation = [[Place alloc] init] ;
    toLocation.name = @"toLocation";
    toLocation.description = @"toLocation";
    toLocation.latitude = 31.9824290;
    toLocation.longitude = 118.7449390;
//    [mapView showRouteFrom:fromLocation to:toLocation];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressMap:)];
    longPress.minimumPressDuration = 1;
    [mapView addGestureRecognizer:longPress];
    
    [self userCurrentLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)userCurrentLocation {
    self.locmanager = [[CLLocationManager alloc] init];
    [self.locmanager requestAlwaysAuthorization];
    self.locmanager.delegate = self;
    [self.locmanager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.locmanager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocationCoordinate2D loc = [newLocation coordinate];
    
    toLocation = [[Place alloc] init] ;
    toLocation.name = @"toLocation";
    toLocation.description = @"toLocation";
    toLocation.latitude = loc.latitude;
    toLocation.longitude = loc.longitude;
    if (fromLocation.longitude > 0 && fromLocation.latitude > 0 && toLocation.longitude > 0 && toLocation.latitude > 0) {
        [mapView showRouteFrom:fromLocation to:toLocation];
    }
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateSpan span = MKCoordinateSpanMake(0.01, 0.01);
    MKCoordinateRegion region = MKCoordinateRegionMake(userLocation.coordinate, span);
    [self.mapView setRegion:region];
    
    fromLocation.longitude = userLocation.coordinate.longitude;
    fromLocation.latitude = userLocation.coordinate.latitude;
    fromLocation.description = @"fromLocation";
    fromLocation.name = @"fromLocation";
    
    self.mapView.mapView.centerCoordinate = userLocation.location.coordinate;
}

- (void)longPressMap:(UILongPressGestureRecognizer*)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"longPressed Map");
        CGPoint touchPoint = [recognizer locationInView:mapView.mapView];
        CLLocationCoordinate2D coordinate = [mapView.mapView convertPoint:touchPoint toCoordinateFromView:mapView.mapView];
        toLocation.longitude = coordinate.longitude;
        toLocation.latitude = coordinate.latitude;
        [mapView showRouteFrom:fromLocation to:toLocation];
    }
}

@end
